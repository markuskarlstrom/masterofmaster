
typedef enum {
    LEDCONTROL_LED1 = 0,
    LEDCONTROL_LED2,
    LEDCONTROL_LED3,
    LEDCONTROL_LED4,
    LEDCONTROL_LED5,
    LEDCONTROL_START_BTN_LED,
} LEDCONTROL_LEDS_t;


void LEDCONTROL_init(void);
void LEDCONTROL_setBrightness(uint16_t brightness);
uint16_t LEDCONTROL_getBrightness(void);
void LEDCONTROL_switchOnLedNbrWithSetBrightness(LEDCONTROL_LEDS_t ledNbr);
void LEDCONTROL_switchOnLedNbr(LEDCONTROL_LEDS_t ledNbr, uint16_t brightness);
void LEDCONTROL_switchOffLedNbr(LEDCONTROL_LEDS_t ledNbr);
void LEDCONTROL_setBrightnessPercent(uint16_t brightnessPercent);
uint16_t LEDCONTROL_getBrightnessPercent(void);