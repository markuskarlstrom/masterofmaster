#include <stdint.h>

#include "config.h"
#include "driver/ledc.h"
#include "LEDCONTROL/ledcontrol.h"

#define LEDC_CH_NUM       (6)

static struct
{
    uint16_t ledBrightness;
    ledc_timer_config_t ledc_timer;
    ledc_channel_config_t ledc_channel[LEDC_CH_NUM];
} priv = {
    .ledBrightness = 1023,
    .ledc_timer = {
        .duty_resolution = LEDC_TIMER_10_BIT, // resolution of PWM duty
        .freq_hz = 5000,                      // frequency of PWM signal
        .speed_mode = LEDC_HIGH_SPEED_MODE,           // timer mode
        .timer_num = LEDC_TIMER_0,            // timer index
        .clk_cfg = LEDC_AUTO_CLK,              // Auto select the source clock
    },
    .ledc_channel = {
        {
            .channel    = LEDC_CHANNEL_0,
            .duty       = 0,
            .gpio_num   = LED1_GPIO_PIN,
            .speed_mode = LEDC_HIGH_SPEED_MODE,
            .hpoint     = 0,
            .timer_sel  = LEDC_TIMER_0,
        },
        {
            .channel    = LEDC_CHANNEL_1,
            .duty       = 0,
            .gpio_num   = LED2_GPIO_PIN,
            .speed_mode = LEDC_HIGH_SPEED_MODE,
            .hpoint     = 0,
            .timer_sel  = LEDC_TIMER_0,
        },
        {
            .channel    = LEDC_CHANNEL_2,
            .duty       = 0,
            .gpio_num   = LED3_GPIO_PIN,
            .speed_mode = LEDC_HIGH_SPEED_MODE,
            .hpoint     = 0,
            .timer_sel  = LEDC_TIMER_0,
        },
        {
            .channel    = LEDC_CHANNEL_3,
            .duty       = 0,
            .gpio_num   = LED4_GPIO_PIN,
            .speed_mode = LEDC_HIGH_SPEED_MODE,
            .hpoint     = 0,
            .timer_sel  = LEDC_TIMER_0,
        },
        {
            .channel    = LEDC_CHANNEL_4,
            .duty       = 0,
            .gpio_num   = LED5_GPIO_PIN,
            .speed_mode = LEDC_HIGH_SPEED_MODE,
            .hpoint     = 0,
            .timer_sel  = LEDC_TIMER_0,
        },
        {
            .channel    = LEDC_CHANNEL_5,
            .duty       = 0,
            .gpio_num   = BUTTON_START_LED_PIN,
            .speed_mode = LEDC_HIGH_SPEED_MODE,
            .hpoint     = 0,
            .timer_sel  = LEDC_TIMER_0,
        },
    }
};

void LEDCONTROL_init(void)
{
    // Set configuration of timer0 for high speed channels
    ledc_timer_config(&priv.ledc_timer);

    for (int i = 0; i < LEDC_CH_NUM; i++)
    {
        ledc_channel_config(&priv.ledc_channel[i]);
        ledc_set_duty(priv.ledc_channel[i].speed_mode, priv.ledc_channel[i].channel, 0);
        ledc_update_duty(priv.ledc_channel[i].speed_mode, priv.ledc_channel[i].channel);
    }

}



void LEDCONTROL_setBrightness(uint16_t brightness)
{
    priv.ledBrightness = brightness;
}

uint16_t LEDCONTROL_getBrightness(void)
{
    return priv.ledBrightness;
}

void LEDCONTROL_setBrightnessPercent(uint16_t brightnessPercent)
{
   if(brightnessPercent > 100)
   {
       brightnessPercent = 100;
   }
   else if (brightnessPercent <= 1)
   {
       brightnessPercent = 1;
   }
   float scaleFactor = (float)brightnessPercent / 100;

    priv.ledBrightness = 1023 * scaleFactor;
}

uint16_t LEDCONTROL_getBrightnessPercent(void)
{
    float percent = (float)priv.ledBrightness/ 1023.0f;
    return (uint16_t)(percent*100.0f);
}

void LEDCONTROL_switchOnLedNbrWithSetBrightness(LEDCONTROL_LEDS_t ledNbr)
{
    switch (ledNbr) 
    {
        case LEDCONTROL_LED1:
            ledc_set_duty(priv.ledc_channel[0].speed_mode, priv.ledc_channel[0].channel, priv.ledBrightness);
            ledc_update_duty(priv.ledc_channel[0].speed_mode, priv.ledc_channel[0].channel);
        break; 
        case LEDCONTROL_LED2:
            ledc_set_duty(priv.ledc_channel[1].speed_mode, priv.ledc_channel[1].channel, priv.ledBrightness);
            ledc_update_duty(priv.ledc_channel[1].speed_mode, priv.ledc_channel[1].channel);
        break; 
        case LEDCONTROL_LED3:
            ledc_set_duty(priv.ledc_channel[2].speed_mode, priv.ledc_channel[2].channel, priv.ledBrightness);
            ledc_update_duty(priv.ledc_channel[2].speed_mode, priv.ledc_channel[2].channel);
        break; 
        case LEDCONTROL_LED4:
            ledc_set_duty(priv.ledc_channel[3].speed_mode, priv.ledc_channel[3].channel, priv.ledBrightness);
            ledc_update_duty(priv.ledc_channel[3].speed_mode, priv.ledc_channel[3].channel);
        break; 
        case LEDCONTROL_LED5:
            ledc_set_duty(priv.ledc_channel[4].speed_mode, priv.ledc_channel[4].channel, priv.ledBrightness);
            ledc_update_duty(priv.ledc_channel[4].speed_mode, priv.ledc_channel[4].channel);
        break; 
        case LEDCONTROL_START_BTN_LED:
            ledc_set_duty(priv.ledc_channel[5].speed_mode, priv.ledc_channel[5].channel, priv.ledBrightness);
            ledc_update_duty(priv.ledc_channel[5].speed_mode, priv.ledc_channel[5].channel);
        break; 
        default:
        break;
    }
    
}

void LEDCONTROL_switchOnLedNbr(LEDCONTROL_LEDS_t ledNbr, uint16_t brightness)
{
    switch (ledNbr) 
    {
        case LEDCONTROL_LED1:
            ledc_set_duty(priv.ledc_channel[0].speed_mode, priv.ledc_channel[0].channel, brightness);
            ledc_update_duty(priv.ledc_channel[0].speed_mode, priv.ledc_channel[0].channel);
        break; 
        case LEDCONTROL_LED2:
            ledc_set_duty(priv.ledc_channel[1].speed_mode, priv.ledc_channel[1].channel, brightness);
            ledc_update_duty(priv.ledc_channel[1].speed_mode, priv.ledc_channel[1].channel);
        break; 
        case LEDCONTROL_LED3:
            ledc_set_duty(priv.ledc_channel[2].speed_mode, priv.ledc_channel[2].channel, brightness);
            ledc_update_duty(priv.ledc_channel[2].speed_mode, priv.ledc_channel[2].channel);
        break; 
        case LEDCONTROL_LED4:
            ledc_set_duty(priv.ledc_channel[3].speed_mode, priv.ledc_channel[3].channel, brightness);
            ledc_update_duty(priv.ledc_channel[3].speed_mode, priv.ledc_channel[3].channel);
        break; 
        case LEDCONTROL_LED5:
            ledc_set_duty(priv.ledc_channel[4].speed_mode, priv.ledc_channel[4].channel, brightness);
            ledc_update_duty(priv.ledc_channel[4].speed_mode, priv.ledc_channel[4].channel);
        break; 
        case LEDCONTROL_START_BTN_LED:
            ledc_set_duty(priv.ledc_channel[5].speed_mode, priv.ledc_channel[5].channel, brightness);
            ledc_update_duty(priv.ledc_channel[5].speed_mode, priv.ledc_channel[5].channel);
        break; 
        default:
        break;
    }
    
}

void LEDCONTROL_switchOffLedNbr(LEDCONTROL_LEDS_t ledNbr)
{
        switch (ledNbr) 
    {
        case LEDCONTROL_LED1:
            ledc_set_duty(priv.ledc_channel[0].speed_mode, priv.ledc_channel[0].channel, 0);
            ledc_update_duty(priv.ledc_channel[0].speed_mode, priv.ledc_channel[0].channel);
        break; 
        case LEDCONTROL_LED2:
            ledc_set_duty(priv.ledc_channel[1].speed_mode, priv.ledc_channel[1].channel, 0);
            ledc_update_duty(priv.ledc_channel[1].speed_mode, priv.ledc_channel[1].channel);
        break; 
        case LEDCONTROL_LED3:
            ledc_set_duty(priv.ledc_channel[2].speed_mode, priv.ledc_channel[2].channel, 0);
            ledc_update_duty(priv.ledc_channel[2].speed_mode, priv.ledc_channel[2].channel);
        break; 
        case LEDCONTROL_LED4:
            ledc_set_duty(priv.ledc_channel[3].speed_mode, priv.ledc_channel[3].channel, 0);
            ledc_update_duty(priv.ledc_channel[3].speed_mode, priv.ledc_channel[3].channel);
        break; 
        case LEDCONTROL_LED5:
            ledc_set_duty(priv.ledc_channel[4].speed_mode, priv.ledc_channel[4].channel, 0);
            ledc_update_duty(priv.ledc_channel[4].speed_mode, priv.ledc_channel[4].channel);
        break;
        case LEDCONTROL_START_BTN_LED:
            ledc_set_duty(priv.ledc_channel[5].speed_mode, priv.ledc_channel[5].channel, 0);
            ledc_update_duty(priv.ledc_channel[5].speed_mode, priv.ledc_channel[5].channel);
        break;  
        default:
        break;
    }
}
