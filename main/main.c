/* Blink Example

   This example code is in the Public Domain (or CC0 licensed, at your option.)

   Unless required by applicable law or agreed to in writing, this
   software is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR
   CONDITIONS OF ANY KIND, either express or implied.
*/
#include <stdio.h>

#include "config.h"

#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "driver/gpio.h"
#include "sdkconfig.h"
#include "GAMEHANDLER/gamehandler.h"
#include "BUTTONHANDLER/buttonhandler.h"
#include "LEDCONTROL/ledcontrol.h"
#include "LIGHTFESTIVAL/lightfestival.h"
#include "WEBHANDLER/webhandler.h"

/* Can use project configuration menu (idf.py menuconfig) to choose the GPIO to blink,
   or you can edit the following line and set a number here.
*/
#define BLINK_GPIO 16

static void initBoard(void)
{
    // LED Pins --------------------------------------------------
    gpio_pad_select_gpio(LED1_GPIO_PIN);
    gpio_pad_select_gpio(LED2_GPIO_PIN);
    gpio_pad_select_gpio(LED3_GPIO_PIN);
    gpio_pad_select_gpio(LED4_GPIO_PIN);
    gpio_pad_select_gpio(LED5_GPIO_PIN);

    gpio_set_direction(LED1_GPIO_PIN, GPIO_MODE_OUTPUT);
    gpio_set_direction(LED2_GPIO_PIN, GPIO_MODE_OUTPUT);
    gpio_set_direction(LED3_GPIO_PIN, GPIO_MODE_OUTPUT);
    gpio_set_direction(LED4_GPIO_PIN, GPIO_MODE_OUTPUT);
    gpio_set_direction(LED5_GPIO_PIN, GPIO_MODE_OUTPUT);

    gpio_set_level(LED1_GPIO_PIN, 0);
    gpio_set_level(LED2_GPIO_PIN, 0);
    gpio_set_level(LED3_GPIO_PIN, 0);
    gpio_set_level(LED4_GPIO_PIN, 0);
    gpio_set_level(LED5_GPIO_PIN, 0);

    // Button Pins ------------------------------------------------
    gpio_pad_select_gpio(BUTTON_START_PIN);
    gpio_pad_select_gpio(BUTTON_START_LED_PIN);

    gpio_set_direction(BUTTON_START_PIN, GPIO_MODE_INPUT);
    gpio_set_direction(BUTTON_START_LED_PIN, GPIO_MODE_OUTPUT);

    gpio_set_level(BUTTON_START_LED_PIN, 1);
}


void app_main(void)
{
    initBoard();

    LEDCONTROL_init();
    GAMEHANDLER_init();
    BUTTONHANDLER_init();
    WEBHANDLER_init();
    LIGHTFESTIVAL_init();
    while(1) {
        // Do nothing
        vTaskDelay(100 / portTICK_PERIOD_MS);
    }
}
