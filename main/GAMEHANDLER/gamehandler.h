

void GAMEHANDLER_init();
void GAMEHANDLER_requestStart(void);
bool GAMEHANDLER_isGameInProgress(void);
uint32_t GAMEHANDLER_getMinGameTimeSec(void);
uint32_t GAMEHANDLER_getMaxGameTimeSec(void);
void GAMEHANDLER_setMinGameTimeSec(uint32_t value);
void GAMEHANDLER_setMaxGameTimeSec(uint32_t value);