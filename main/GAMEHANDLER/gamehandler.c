// Includes ---------------------------------------
#include <stddef.h>
#include <stdint.h>
#include <stdbool.h>

#include "config.h"

#include "gamehandler.h"
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "driver/gpio.h"
#include "esp_timer.h"
#include "esp_system.h"
#include "math.h"

#include "LEDCONTROL/ledcontrol.h"


// Defines ----------------------------------------

typedef struct {
    uint32_t minTimeMilliSeconds;
    uint32_t maxTimeMilliSeconds;
} gameSettings_t;

typedef struct {
    bool isSessionvalid;
    uint32_t endTime_msec;
    uint32_t lampNbrToSwitchOff;
}gameSession_t;

// Private Data
static struct
{
    gameSettings_t gameSettings;
    gameSession_t gameSession;
    bool isStartRequested;
}priv;


// Private Functions ------------------------------

void switchOfLED(uint32_t ledNr)
{
    switch (ledNr)
    {
        case 1:
            LEDCONTROL_switchOffLedNbr(LEDCONTROL_LED1);
            break;
        case 2:
            LEDCONTROL_switchOffLedNbr(LEDCONTROL_LED2);
            break;
        case 3:
            LEDCONTROL_switchOffLedNbr(LEDCONTROL_LED3);
            break;
        case 4:
            LEDCONTROL_switchOffLedNbr(LEDCONTROL_LED4);
            break;
        case 5:
            LEDCONTROL_switchOffLedNbr(LEDCONTROL_LED5);
            break;
        default:
            // Do nothing
            break;
    }

    printf("Sitch of LED: %d \n", ledNr);
}

void startGameLEDPattern(void)
{
    const TickType_t xDelay = 500 / portTICK_PERIOD_MS;   // 1 sec

    LEDCONTROL_switchOffLedNbr(LEDCONTROL_LED1);
    LEDCONTROL_switchOffLedNbr(LEDCONTROL_LED2);
    LEDCONTROL_switchOffLedNbr(LEDCONTROL_LED3);
    LEDCONTROL_switchOffLedNbr(LEDCONTROL_LED4);
    LEDCONTROL_switchOffLedNbr(LEDCONTROL_LED5);

    vTaskDelay( xDelay );
    LEDCONTROL_switchOnLedNbrWithSetBrightness(LEDCONTROL_LED1);
    
    vTaskDelay( xDelay );
    LEDCONTROL_switchOnLedNbrWithSetBrightness(LEDCONTROL_LED2);

    vTaskDelay( xDelay );
    LEDCONTROL_switchOnLedNbrWithSetBrightness(LEDCONTROL_LED3);

    vTaskDelay( xDelay );
    LEDCONTROL_switchOnLedNbrWithSetBrightness(LEDCONTROL_LED4);

    vTaskDelay( xDelay );
    LEDCONTROL_switchOnLedNbrWithSetBrightness(LEDCONTROL_LED5);
}

void startGame(void)
{
    printf("New Game! \n");
    // Show Start Seq
    startGameLEDPattern();

    // Create Random which lamp to switch off
    priv.gameSession.lampNbrToSwitchOff = (esp_random() % 5 ) + 1;

    // Create a random time to when trigger lamp
    uint32_t randomNumber_msec = esp_random();
    uint32_t timeToAdd_msec = ((randomNumber_msec % (priv.gameSettings.maxTimeMilliSeconds - priv.gameSettings.minTimeMilliSeconds)) + priv.gameSettings.minTimeMilliSeconds) * 1000;

    priv.gameSession.endTime_msec = (uint32_t)esp_timer_get_time() + timeToAdd_msec;
    priv.gameSession.isSessionvalid = true;

    priv.isStartRequested = false;

    printf("New Game Session: \n");
    printf("Current Time: %d \n", (uint32_t)esp_timer_get_time());
    printf("Add Time: %d \n", timeToAdd_msec);
    printf("End Time: %d \n", priv.gameSession.endTime_msec);
    printf("Lamp Nbr: %d \n", priv.gameSession.lampNbrToSwitchOff);

}

void gameThread(void *args)
{
  const TickType_t xDelay = 10 / portTICK_PERIOD_MS;   // 100 ms


  for(;;)
  {
    
    if (priv.isStartRequested)
    {
        startGame();
    }

    if (priv.gameSession.isSessionvalid)
    {
        if ((uint32_t)esp_timer_get_time() > priv.gameSession.endTime_msec)
        {
            switchOfLED(priv.gameSession.lampNbrToSwitchOff);
            vTaskDelay( 1000 / portTICK_PERIOD_MS );
            priv.gameSession.isSessionvalid = false;
        }
    }
    vTaskDelay( xDelay );
  }
}

// Functions --------------------------------------
void GAMEHANDLER_init(void)
{
    priv.gameSettings.minTimeMilliSeconds = 15000;
    priv.gameSettings.maxTimeMilliSeconds = 45000;
    
    xTaskCreate(&gameThread, "GAMETHREAD", STACK_SIZE_GAMEHANDLER, NULL, PRIO_GAMEHANDLER, NULL);
}

void GAMEHANDLER_requestStart(void)
{
    priv.isStartRequested = true;
}

bool GAMEHANDLER_isGameInProgress(void)
{
    return priv.gameSession.isSessionvalid;
}

uint32_t GAMEHANDLER_getMinGameTimeSec(void)
{
    return priv.gameSettings.minTimeMilliSeconds / 1000;
}

uint32_t GAMEHANDLER_getMaxGameTimeSec(void)
{
    return priv.gameSettings.maxTimeMilliSeconds / 1000;
}

void GAMEHANDLER_setMinGameTimeSec(uint32_t value)
{
    priv.gameSettings.minTimeMilliSeconds = value * 1000;
}

void GAMEHANDLER_setMaxGameTimeSec(uint32_t value)
{
    priv.gameSettings.maxTimeMilliSeconds = value * 1000;
}