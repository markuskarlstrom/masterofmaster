

typedef enum {
    LIGHTSHOW_PONG = 0,
    LIGHTSHOW_STROBE10HZ,
    LIGHTSHOW_FADING,
}LIGHTSHOWS_t;

void LIGHTFESTIVAL_init(void);

void LIGHTFESTIVAL_startShow(LIGHTSHOWS_t show);
void LIGHTFESTIVAL_stopShow(void);