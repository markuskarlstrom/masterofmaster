#include <stddef.h>
#include <stdint.h>
#include <stdbool.h>

#include "config.h"

#include "lightfestival.h"
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "esp_timer.h"
#include "esp_system.h"
#include "math.h"
#include "esp_log.h"

#include "LEDCONTROL/ledcontrol.h"

typedef struct
{
    int16_t startBright;
    int16_t endBright;
} ledPair_t;

typedef struct 
{
    ledPair_t led1;
    ledPair_t led2;
    ledPair_t led3;
    ledPair_t led4;
    ledPair_t led5;
    float startTime_sec;
    float stopTime_sec;
} lightRow_t;

typedef struct 
{
    uint16_t nbrOfRows;
    lightRow_t rows[20];
}show_t;


static show_t pong =
{
    .nbrOfRows = 8,
    .rows = {
        { {1000, 1000}, {0, 0}, {0, 0}, {0, 0}, {0, 0},  0.0, 0.150 },
        { {0, 0}, {1000, 1000}, {0, 0}, {0, 0}, {0, 0},  0.150, 0.300 },
        { {0, 0}, {0, 0}, {1000, 1000}, {0, 0}, {0, 0},  0.300, 0.450 },
        { {0, 0}, {0, 0}, {0, 0}, {1000, 1000}, {0, 0},  0.450, 0.600 },
        { {0, 0}, {0, 0}, {0, 0}, {0, 0}, {1000, 1000},  0.600, 0.750 },
        { {0, 0}, {0, 0}, {0, 0}, {1000, 1000}, {0, 0},  0.750, 0.900 },
        { {0, 0}, {0, 0}, {1000, 1000}, {0, 0}, {0, 0},  0.900, 1.050 },
        { {0, 0}, {1000, 1000}, {0, 0}, {0, 0}, {0, 0},  1.050, 1.200 },
    }
};

static show_t strobe10Hz =
{
    .nbrOfRows = 2,
    .rows = {
        { {1000, 1000}, {1000, 1000}, {1000, 1000}, {1000, 1000}, {1000, 1000},  0, 0.010 },
        { {0, 0}, {0, 0}, {0, 0}, {0, 0}, {0, 0},  0.010, 0.190 },
    }
};


static show_t fading =
{
    .nbrOfRows = 3,
    .rows = {
        { {1000, 1000}, {1000, 1000}, {1000, 1000}, {1000, 1000}, {1000, 1000},  0, 0.500 },
        { {1000, 0}, {1000, 0}, {1000, 0}, {1000, 0}, {1000, 0},  0.500, 3.000 },
        { {0, 0}, {0, 0}, {0, 0}, {0, 0}, {0, 0},  3.000, 5.000 },
    }
};


static struct {
    bool isLightRequested;
    float startTime;
    show_t *selectedShow
}priv;


static float getTimeSec(void)
{
    uint32_t timeUs = (uint32_t)esp_timer_get_time();
    uint32_t timeMs = timeUs/1000;
    float timeS = (float)timeMs/1000.0f;
    return timeS;
}


static uint16_t getLinearValue(ledPair_t pair, float startTime, float endTime, float currTime)
{
    int16_t x1 = pair.startBright;
    int16_t x2 = pair.endBright;

    float val = (((float)x2 - (float)x1) / (endTime - startTime)) * (currTime - startTime) + (float)x1;

    if (val > 1023.0)
    {
        val = 1023.0;
    } else if (val < 0.0)
    {
        val = 0.0; 
    }
    return (uint16_t)trunc(val);
}



static void handleStuff(show_t *show)
{
    float timeFromStart = getTimeSec() - priv.startTime;

    uint16_t currRow = 0;

    for (currRow = 0; currRow < show->nbrOfRows; currRow++)
    {
        if (timeFromStart < show->rows[currRow].stopTime_sec)
        {
            break;
        }
    }

    if (currRow == show->nbrOfRows)
    {
        currRow = 0;
        priv.startTime = getTimeSec();
    }
    float startTime = show->rows[currRow].startTime_sec;
    
    float endTime = show->rows[currRow].stopTime_sec;

    
    LEDCONTROL_switchOnLedNbr(LEDCONTROL_LED1, getLinearValue(show->rows[currRow].led1, startTime, endTime, timeFromStart ));
    LEDCONTROL_switchOnLedNbr(LEDCONTROL_LED2, getLinearValue(show->rows[currRow].led2, startTime, endTime, timeFromStart ));
    LEDCONTROL_switchOnLedNbr(LEDCONTROL_LED3, getLinearValue(show->rows[currRow].led3, startTime, endTime, timeFromStart ));
    LEDCONTROL_switchOnLedNbr(LEDCONTROL_LED4, getLinearValue(show->rows[currRow].led4, startTime, endTime, timeFromStart ));
    LEDCONTROL_switchOnLedNbr(LEDCONTROL_LED5, getLinearValue(show->rows[currRow].led5, startTime, endTime, timeFromStart ));
}

void startLightThread(void *args)
{
    const TickType_t xDelay = 10 / portTICK_PERIOD_MS;   // 33 ms - 30 fps

    for(;;)
    {
      if (priv.isLightRequested)
      {
        handleStuff(priv.selectedShow);
      }

        vTaskDelay( xDelay );
    }
}

// Functions --------------------------------------

void LIGHTFESTIVAL_init(void)
{
    xTaskCreate(&startLightThread, "LIGHTTHREAD", STACK_SIZE_LIGHTFESTIVAL, NULL, PRIO_LIGHTFESTIVAL, NULL);
}

void LIGHTFESTIVAL_startShow(LIGHTSHOWS_t show)
{
    switch (show)
    {
        case LIGHTSHOW_PONG:
            priv.selectedShow = &pong;
        break;
        case LIGHTSHOW_STROBE10HZ:
            priv.selectedShow = &strobe10Hz;
        break;
        case LIGHTSHOW_FADING:
            priv.selectedShow = &fading;
        break;
        default:
            priv.selectedShow = &pong;
        break;
    }
    
    ESP_LOGI("Festival","%d", show);
    priv.startTime = getTimeSec();
    priv.isLightRequested = true;
}

void LIGHTFESTIVAL_stopShow(void)
{
    priv.isLightRequested = false;
}