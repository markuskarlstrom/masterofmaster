// Includes ---------------------------------------
#include <stddef.h>
#include <stdint.h>
#include <stdbool.h>
#include <string.h>

#include "config.h"

#include "esp_wifi.h"
#include "esp_system.h"
#include "esp_event.h"
#include "esp_log.h"
#include "nvs_flash.h"
#include <esp_https_server.h>
#include "esp_vfs.h"
#include "cJSON.h"

#include "GAMEHANDLER/gamehandler.h"
#include "LEDCONTROL/ledcontrol.h"
#include "LIGHTFESTIVAL/lightfestival.h"
#include "WEBHANDLER/webhandler.h"
#include "WEBHANDLER/website.h"

// Defines -----------------------------------------
#define SOFT_AP_SSID "MasterOfMasters"

#define SOFT_AP_IP_ADDRESS_1 192
#define SOFT_AP_IP_ADDRESS_2 168
#define SOFT_AP_IP_ADDRESS_3 1
#define SOFT_AP_IP_ADDRESS_4 1
 
#define SOFT_AP_GW_ADDRESS_1 192
#define SOFT_AP_GW_ADDRESS_2 168
#define SOFT_AP_GW_ADDRESS_3 1
#define SOFT_AP_GW_ADDRESS_4 2

#define SOFT_AP_NM_ADDRESS_1 255
#define SOFT_AP_NM_ADDRESS_2 255
#define SOFT_AP_NM_ADDRESS_3 255
#define SOFT_AP_NM_ADDRESS_4 0

#define SERVER_PORT 80

#define SCRATCH_BUFSIZE (10240)

static httpd_handle_t httpServerInstance = NULL;
 
static esp_err_t home_get_handler(httpd_req_t *req){
    ESP_LOGI("HANDLER","This is the handler for the <%s> URI", req->uri);
    httpd_resp_set_type(req, "text/html");
    httpd_resp_send(req, HOME_WEB_PAGE, HTTPD_RESP_USE_STRLEN);
    
    return ESP_OK;
}

static esp_err_t game_data_get_handler(httpd_req_t *req)
{
    char page[2048] = {0};
    snprintf(page, 2048, GAME_SETTING_WEB_PAGE, LEDCONTROL_getBrightnessPercent(), GAMEHANDLER_getMinGameTimeSec(), GAMEHANDLER_getMaxGameTimeSec());
    httpd_resp_set_type(req, "text/html");
    httpd_resp_send(req, page, HTTPD_RESP_USE_STRLEN);
 
    return ESP_OK;
}
 
 static esp_err_t manual_get_handler(httpd_req_t *req)
{
    httpd_resp_set_type(req, "text/html");
    httpd_resp_send(req, MANUAL_WEB_PAGE, HTTPD_RESP_USE_STRLEN);
 
    return ESP_OK;
}

 static esp_err_t festival_get_handler(httpd_req_t *req)
{
    httpd_resp_set_type(req, "text/html");
    httpd_resp_send(req, FESTIVAL_WEB_PAGE, HTTPD_RESP_USE_STRLEN);
 
    return ESP_OK;
}

static esp_err_t game_data_post_handler(httpd_req_t *req)
{
    int total_len = req->content_len;
    int cur_len = 0;
    char buf[256] = {0};
    int received = 0;
    if (total_len >= SCRATCH_BUFSIZE) {
        /* Respond with 500 Internal Server Error */
        httpd_resp_send_err(req, HTTPD_500_INTERNAL_SERVER_ERROR, "content too long");
        return ESP_FAIL;
    }
    while (cur_len < total_len) {
        received = httpd_req_recv(req, buf + cur_len, total_len);
        if (received <= 0) {
            /* Respond with 500 Internal Server Error */
            httpd_resp_send_err(req, HTTPD_500_INTERNAL_SERVER_ERROR, "Failed to post control value");
            return ESP_FAIL;
        }
        cur_len += received;
    }
    buf[total_len] = '\0';
    char *ptr = buf;

    while (*ptr != '\0')
    {
        if (*ptr == '&' || *ptr == '=')
        {
            *ptr = ' ';
        }
        ptr++;
    }
    
    unsigned int brightInt;
    unsigned int minInt;
    unsigned int maxInt;
    sscanf(buf, "%*s %u %*s %u %*s %u", &brightInt, &minInt, &maxInt);
    ESP_LOGI("HANDLER","%u, %u, %u", brightInt, minInt, maxInt);

    LEDCONTROL_setBrightnessPercent(brightInt);
    GAMEHANDLER_setMinGameTimeSec(minInt);
    GAMEHANDLER_setMaxGameTimeSec(maxInt);
    
    game_data_get_handler(req);
    return ESP_OK;
}

static esp_err_t manual_post_handler(httpd_req_t *req)
{
    int total_len = req->content_len;
    int cur_len = 0;
    char buf[100] = {0};
    int received = 0;
    if (total_len >= SCRATCH_BUFSIZE) {
        /* Respond with 500 Internal Server Error */
        httpd_resp_send_err(req, HTTPD_500_INTERNAL_SERVER_ERROR, "content too long");
        return ESP_FAIL;
    }
    while (cur_len < total_len) {
        received = httpd_req_recv(req, buf + cur_len, total_len);
        if (received <= 0) {
            /* Respond with 500 Internal Server Error */
            httpd_resp_send_err(req, HTTPD_500_INTERNAL_SERVER_ERROR, "Failed to post control value");
            return ESP_FAIL;
        }
        cur_len += received;
    }
    buf[total_len] = '\0';
    ESP_LOGI("HANDLER","%s", buf);

    char *ptr = buf;
    while (*ptr != '\0')
    {
        if (*ptr == '=')
        {
            *ptr = ' ';
        }
        ptr++;
    }
    
    unsigned int ledNbr;
    sscanf(buf, "%*s %u ", &ledNbr);
    LIGHTFESTIVAL_stopShow();
    switch (ledNbr/10)
    {
     case 1:
        if (ledNbr % 10 == 0)
        {
            LEDCONTROL_switchOffLedNbr(LEDCONTROL_LED1);
        }
        else
        {
            LEDCONTROL_switchOnLedNbrWithSetBrightness(LEDCONTROL_LED1);
        }
     break;
          case 2:
        if (ledNbr % 10 == 0)
        {
            LEDCONTROL_switchOffLedNbr(LEDCONTROL_LED2);
        }
        else
        {
            LEDCONTROL_switchOnLedNbrWithSetBrightness(LEDCONTROL_LED2);
        }
     break;
          case 3:
        if (ledNbr % 10 == 0)
        {
            LEDCONTROL_switchOffLedNbr(LEDCONTROL_LED3);
        }
        else
        {
            LEDCONTROL_switchOnLedNbrWithSetBrightness(LEDCONTROL_LED3);
        }
     break;
          case 4:
        if (ledNbr % 10 == 0)
        {
            LEDCONTROL_switchOffLedNbr(LEDCONTROL_LED4);
        }
        else
        {
            LEDCONTROL_switchOnLedNbrWithSetBrightness(LEDCONTROL_LED4);
        }
     break;
          case 5:
        if (ledNbr % 10 == 0)
        {
            LEDCONTROL_switchOffLedNbr(LEDCONTROL_LED5);
        }
        else
        {
            LEDCONTROL_switchOnLedNbrWithSetBrightness(LEDCONTROL_LED5);
        }
     break;
          case 6:
        if (ledNbr % 10 == 0)
        {
            LEDCONTROL_switchOffLedNbr(LEDCONTROL_START_BTN_LED);
        }
        else
        {
            LEDCONTROL_switchOnLedNbrWithSetBrightness(LEDCONTROL_START_BTN_LED);
        }
     break;
            case 10:
        if (ledNbr % 10 == 0)
        {
            LEDCONTROL_switchOffLedNbr(LEDCONTROL_LED1);
            LEDCONTROL_switchOffLedNbr(LEDCONTROL_LED2);
            LEDCONTROL_switchOffLedNbr(LEDCONTROL_LED3);
            LEDCONTROL_switchOffLedNbr(LEDCONTROL_LED4);
            LEDCONTROL_switchOffLedNbr(LEDCONTROL_LED5);
        }
        else
        {
            LEDCONTROL_switchOnLedNbrWithSetBrightness(LEDCONTROL_LED1);
            LEDCONTROL_switchOnLedNbrWithSetBrightness(LEDCONTROL_LED2);
            LEDCONTROL_switchOnLedNbrWithSetBrightness(LEDCONTROL_LED3);
            LEDCONTROL_switchOnLedNbrWithSetBrightness(LEDCONTROL_LED4);
            LEDCONTROL_switchOnLedNbrWithSetBrightness(LEDCONTROL_LED5);
        }
     break;
    }

    manual_get_handler(req);
    return ESP_OK;
}

static esp_err_t festival_post_handler(httpd_req_t *req)
{
    int total_len = req->content_len;
    int cur_len = 0;
    char buf[100] = {0};
    int received = 0;
    if (total_len >= SCRATCH_BUFSIZE) {
        /* Respond with 500 Internal Server Error */
        httpd_resp_send_err(req, HTTPD_500_INTERNAL_SERVER_ERROR, "content too long");
        return ESP_FAIL;
    }
    while (cur_len < total_len) {
        received = httpd_req_recv(req, buf + cur_len, total_len);
        if (received <= 0) {
            /* Respond with 500 Internal Server Error */
            httpd_resp_send_err(req, HTTPD_500_INTERNAL_SERVER_ERROR, "Failed to post control value");
            return ESP_FAIL;
        }
        cur_len += received;
    }
    buf[total_len] = '\0';
    ESP_LOGI("HANDLER","%s", buf);

    char *ptr = buf;
    while (*ptr != '\0')
    {
        if (*ptr == '=')
        {
            *ptr = ' ';
        }
        ptr++;
    }
    
    unsigned int showNbr;
    sscanf(buf, "%*s %u ", &showNbr);
    switch (showNbr)
    {
        case 0:
            LIGHTFESTIVAL_startShow(LIGHTSHOW_PONG);
        break;
        case 1:
            LIGHTFESTIVAL_startShow(LIGHTSHOW_STROBE10HZ);
        break;
        case 2:
            LIGHTFESTIVAL_startShow(LIGHTSHOW_FADING);
        break;
        case 3:
        break;
        case 4:
     break;
    }

    festival_get_handler(req);
    return ESP_OK;
}

static httpd_uri_t home_get_uri = {
        .uri = "/home",
        .method = HTTP_GET,
        .handler = home_get_handler,
        .user_ctx = NULL
};  

/* URI handler for game data */
static httpd_uri_t game_data_get_uri = {
        .uri = "/api/gameData",
        .method = HTTP_GET,
        .handler = game_data_get_handler,
        .user_ctx = NULL
};  

static httpd_uri_t game_data_post_uri = {
    .uri = "/api/gameData",
    .method = HTTP_POST,
    .handler = game_data_post_handler,
    .user_ctx = NULL
};

/* URI handler for manual game*/
static httpd_uri_t manual_get_uri = {
        .uri = "/api/manual",
        .method = HTTP_GET,
        .handler = manual_get_handler,
        .user_ctx = NULL
};  

static httpd_uri_t manual_post_uri = {
    .uri = "/api/manual",
    .method = HTTP_POST,
    .handler = manual_post_handler,
    .user_ctx = NULL
};

/* URI handler for manual game*/
static httpd_uri_t festival_get_uri = {
        .uri = "/api/festival",
        .method = HTTP_GET,
        .handler = festival_get_handler,
        .user_ctx = NULL
};  

static httpd_uri_t festival_post_uri = {
    .uri = "/api/festival",
    .method = HTTP_POST,
    .handler = festival_post_handler,
    .user_ctx = NULL
};
 
static void startHttpServer(void){
    httpd_config_t httpServerConfiguration = HTTPD_DEFAULT_CONFIG();
    httpServerConfiguration.server_port = SERVER_PORT;
    if(httpd_start(&httpServerInstance, &httpServerConfiguration) == ESP_OK){
        ESP_ERROR_CHECK(httpd_register_uri_handler(httpServerInstance, &game_data_get_uri));
        ESP_ERROR_CHECK(httpd_register_uri_handler(httpServerInstance, &game_data_post_uri));
        ESP_ERROR_CHECK(httpd_register_uri_handler(httpServerInstance, &home_get_uri));
        ESP_ERROR_CHECK(httpd_register_uri_handler(httpServerInstance, &manual_get_uri));
        ESP_ERROR_CHECK(httpd_register_uri_handler(httpServerInstance, &manual_post_uri));
        ESP_ERROR_CHECK(httpd_register_uri_handler(httpServerInstance, &festival_get_uri));
        ESP_ERROR_CHECK(httpd_register_uri_handler(httpServerInstance, &festival_post_uri));
    }
}
 
static void stopHttpServer(void){
    if(httpServerInstance != NULL){
        ESP_ERROR_CHECK(httpd_stop(httpServerInstance));
    }
}
 
static esp_err_t wifiEventHandler(void* userParameter, system_event_t *event) {
    switch(event->event_id){
    case SYSTEM_EVENT_AP_STACONNECTED:
        startHttpServer();
        break;
    case SYSTEM_EVENT_AP_STADISCONNECTED:
        stopHttpServer();
        break;
    default:
        break;
    }
    return ESP_OK;
}

static void launchSoftAp(void)
{
    // Start AP
    ESP_ERROR_CHECK(nvs_flash_init());
    tcpip_adapter_init();
    ESP_ERROR_CHECK(tcpip_adapter_dhcps_stop(TCPIP_ADAPTER_IF_AP));
    tcpip_adapter_ip_info_t ipAddressInfo;
    memset(&ipAddressInfo, 0, sizeof(ipAddressInfo));
    
    IP4_ADDR(
        &ipAddressInfo.ip,
        SOFT_AP_IP_ADDRESS_1,
        SOFT_AP_IP_ADDRESS_2,
        SOFT_AP_IP_ADDRESS_3,
        SOFT_AP_IP_ADDRESS_4);
    IP4_ADDR(
        &ipAddressInfo.gw,
        SOFT_AP_GW_ADDRESS_1,
        SOFT_AP_GW_ADDRESS_2,
        SOFT_AP_GW_ADDRESS_3,
        SOFT_AP_GW_ADDRESS_4);
    IP4_ADDR(
        &ipAddressInfo.netmask,
        SOFT_AP_NM_ADDRESS_1,
        SOFT_AP_NM_ADDRESS_2,
        SOFT_AP_NM_ADDRESS_3,
        SOFT_AP_NM_ADDRESS_4);

    ESP_ERROR_CHECK(tcpip_adapter_set_ip_info(TCPIP_ADAPTER_IF_AP, &ipAddressInfo));
    ESP_ERROR_CHECK(tcpip_adapter_dhcps_start(TCPIP_ADAPTER_IF_AP));
    ESP_ERROR_CHECK(esp_event_loop_init(wifiEventHandler, NULL));
    wifi_init_config_t wifiConfiguration = WIFI_INIT_CONFIG_DEFAULT();
    ESP_ERROR_CHECK(esp_wifi_init(&wifiConfiguration));
    ESP_ERROR_CHECK(esp_wifi_set_mode(WIFI_MODE_AP));
    wifi_config_t apConfiguration = {
        .ap = {
            .ssid = SOFT_AP_SSID,
            .password = "",
            .ssid_len = 0,
            //.channel = default,
            .authmode = WIFI_AUTH_OPEN,
            .ssid_hidden = 0,
            .max_connection = 4,
            .beacon_interval = 150,
        },
    };
    ESP_ERROR_CHECK(esp_wifi_set_config(WIFI_IF_AP, &apConfiguration));
    ESP_ERROR_CHECK(esp_wifi_set_storage(WIFI_STORAGE_RAM));
    ESP_ERROR_CHECK(esp_wifi_start());
}


void WEBHANDLER_init(void)
{
    launchSoftAp();
}