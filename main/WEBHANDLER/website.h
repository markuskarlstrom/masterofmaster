#define HOME_WEB_PAGE   "<html><head><title>Master Of Masters</title>\
                        <meta name=\"viewport\" content=\"width=device-width, initial-scale=1, shrink-to-fit=no\">\
                        <style> body {text-align:center;font-size: 40px; background-color: #131313; color:#FCF2D8; font-family:arial}\
#rec {background-color: #2B2B2B; color:#FCF2D8; width: 100%; margin:auto; border-radius: 20px;}		\
a { width:40%; color:#FCF2D8; align:center; text-align:center; font-size: 40px; margin:3vh; padding:2vh;\
	border-color:#FCF2D8; border-style:solid; border-width:5px; border-radius: 20px;text-decoration:none;}</style>\
</head>\
<body>\
<div>Master Of Masters</div>\
 <div id='rec'> \
	<div style=\"display:flex; flex-flow:row wrap; justify-content:center;\">\
	<a href=\"/api/gameData\">\
		<div>Game Settings</div>\
	</a>\
	<a href=\"/api/manual\">\
		<div>Manual Game</div>\
	</a>\
	<a href=\"/api/festival\">\
		<div>Light Festival</div>\
	</a>\
	</div>\
</div></body></html>"
                        

#define GAME_SETTING_WEB_PAGE1 "<html>\
                        <h1>Helllo from define</h1>\
                        <div>Brightness : %d <div>\
                        <div>Min : %d <div>\
                        <div>Max : %d <div>\
                        </html>"


#define GAME_SETTING_WEB_PAGE   "<html><head><title>Master Of Masters</title><meta name=\"viewport\" content=\"width=device-width, initial-scale=1, shrink-to-fit=no\">\
<style>\
body {text-align:center;font-size: 40px; background-color: #131313; color:#FCF2D8; font-family:arial}\
#rec {background-color: #2B2B2B; color:#FCF2D8; margin:auto; border-radius: 20px }	\
label {display: inline-block; text-align: right; width:180px;} \
input{width:100px; margin-bottom: 20px;} \
a { color:#FCF2D8; align:center; text-align:center; font-size: 20px; margin:3vh; padding:2vh; text-decoration:none;}</style>\
</head>\
<body>\
<div>Game Settings</div>\
 <div id='rec' style=\"font-size: 20px;\"> \
	<div style=\"display:flex; flex-flow:row wrap; justify-content:center \">\
	<form action=\"/api/gameData\" id=\"form1\" style=\"padding:20px;\">\
		<label for=\"bright\">Brightness:</label> \
	  	<input type=\"number\" id=\"bright\" name=\"bright\" value= %u ><br>\
          <label for=\"min\">Min Game Time [s]:</label>\
	  	<input type=\"number\" id=\"min\" name=\"min\" value= %u ><br>\
		  <label for=\"max\">Max Game Time [s]:</label>\
	  	<input type=\"number\" id=\"max\" name=\"max\" value= %u ><br>\
		<input type=\"submit\" formmethod=\"post\" value=\"Save\" style=\"font-size: 30px; width: 150px; \">\
	</form>\
</div>\
</div>\
<a href=\"/home\">\
	<div>Back</div>\
	</a>\
</body>\
</html>"

#define MANUAL_WEB_PAGE "<html><head><title>Master Of Masters</title>\
<meta name=\"viewport\" content=\"width=device-width, initial-scale=1, shrink-to-fit=no\">\
<style>\
body {text-align:center;font-size: 40px; background-color: #131313; color:#FCF2D8; font-family:arial}\
#rec {background-color: #2B2B2B; color:#FCF2D8; width: 100%; margin:auto; border-radius: 20px;}	\
form div {font-size: 30px;}	\
form div button {font-size: 20px; margin: 10px; width: 80px;}\
a { color:#FCF2D8; align:center; text-align:center; font-size: 20px; margin:3vh; padding:2vh; text-decoration:none;}</style></head><body>\
<div>Manual</div>\
 <div id='rec' style=\"font-size: 20px;\"> \
	<div style=\"display:flex; flex-flow:row wrap; justify-content:center;\">\
	<form action=\"/api/manual\" id=\"form1\" style=\"padding:20px;\" method=\"post\">\
		<div><button name=\"led\" type=\"submit\" value=\"10\">Off</button>\
			LED 1<button name=\"led\" type=\"submit\" value=\"11\">On</button></div>\
		<div><button name=\"led\" type=\"submit\" value=\"20\">Off</button>\
			LED 2<button name=\"led\" type=\"submit\" value=\"21\">On</button></div>\
		<div><button name=\"led\" type=\"submit\" value=\"30\">Off</button>\
			LED 3<button name=\"led\" type=\"submit\" value=\"31\">On</button></div>\
		<div><button name=\"led\" type=\"submit\" value=\"40\">Off</button>\
			LED 4<button name=\"led\" type=\"submit\" value=\"41\">On</button></div>\
		<div><button name=\"led\" type=\"submit\" value=\"50\">Off</button>\
			LED 5<button name=\"led\" type=\"submit\" value=\"51\">On</button></div>\
		<div><button name=\"led\" type=\"submit\" value=\"100\">Off</button>\
			ALL<button name=\"led\" type=\"submit\" value=\"101\">On</button></div>\
		<div><button name=\"led\" type=\"submit\" value=\"60\">Off</button>\
			Start<button name=\"led\" type=\"submit\" value=\"61\">On</button></div>\
	</form></div></div>\
	<a href=\"/home\">\
		<div>Back</div>\
	</a></body></html>"

#define FESTIVAL_WEB_PAGE "<html><head><title>Master Of Masters</title> \
<meta name=\"viewport\" content=\"width=device-width, initial-scale=1, shrink-to-fit=no\">\
<style>\
body {text-align:center;font-size: 40px; background-color: #131313; color:#FCF2D8; font-family:arial}\
#rec {background-color: #2B2B2B; color:#FCF2D8; width: 100%; margin:auto; border-radius: 20px;}	\
form div {font-size: 30px;}	\
form div button {font-size: 20px; margin: 10px; width: 80px;}\
a { color:#FCF2D8; align:center; text-align:center; font-size: 20px; margin:3vh; padding:2vh; text-decoration:none;}</style></head><body>\
<div>Festival</div>\
 <div id='rec' style=\"font-size: 20px;\"> \
	<div style=\"display:flex; flex-flow:row wrap; justify-content:center;\">\
	<form action=\"/api/festival\" id=\"form1\" style=\"padding:20px;\" method=\"post\">\
		<div>Pong<button name=\"show\" type=\"submit\" value=\"0\">Play</button></div>\
		<div>Strobe 10Hz<button name=\"show\" type=\"submit\" value=\"1\">Play</button></div>\
		<div>Fading<button name=\"show\" type=\"submit\" value=\"2\">Play</button></div>\
	</form></div></div>\
	<a href=\"/home\">\
		<div>Back</div>\
	</a></body></html>"
