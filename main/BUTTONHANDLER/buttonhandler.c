// Includes ---------------------------------------
#include <stddef.h>
#include <stdint.h>
#include <stdbool.h>

#include "config.h"

#include "buttonhandler.h"
#include "LEDCONTROL/ledcontrol.h"
#include "GAMEHANDLER/gamehandler.h"
#include "LIGHTFESTIVAL/lightfestival.h"
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "driver/gpio.h"
#include "esp_timer.h"
#include "esp_system.h"
#include "math.h"


// Defines ----------------------------------------
#define SHORT_PRESS_TIME_US     50000
#define LONG_PRESS_TIME_US      1000000
// Private Data -----------------------------------

typedef enum
{
    PRESS_NONE = 0,
    PRESS_SHORT,
    PRESS_LONG,
    RELEASE_SHORT,
    RELEASE_LONG
} BUTTON_ACTIONS_t;

typedef struct 
{
    bool isTimerActive;
    bool buttonState;
    uint32_t startTime;
} buttonFilter_t;

static struct
{
    buttonFilter_t buttonFilter;
} priv;


// Private Functions ------------------------------

uint32_t getDiffTime(uint32_t startTime)
{
    return (uint32_t)esp_timer_get_time() - startTime;
}

bool getRawButtonState(void)
{
    return (bool)gpio_get_level(BUTTON_START_PIN);
}

BUTTON_ACTIONS_t buttonFilter(bool state)
{
    BUTTON_ACTIONS_t action = PRESS_NONE;
    static BUTTON_ACTIONS_t prevAction = PRESS_NONE;

    if (state != priv.buttonFilter.buttonState && !priv.buttonFilter.isTimerActive)
    {
        priv.buttonFilter.startTime = (uint32_t)esp_timer_get_time();
        priv.buttonFilter.isTimerActive = true;
    }
    else if (state != priv.buttonFilter.buttonState && priv.buttonFilter.isTimerActive && getDiffTime(priv.buttonFilter.startTime) > SHORT_PRESS_TIME_US)
    {
        priv.buttonFilter.buttonState = state;
        priv.buttonFilter.isTimerActive = false;
        if (state)
        {
            prevAction = PRESS_SHORT;
            action = PRESS_SHORT;
        }
        else{
            if (prevAction == PRESS_SHORT)
            {
                action = RELEASE_SHORT;
            }
            else if (prevAction == PRESS_LONG)
            {
                action = RELEASE_LONG;
            }
        }
    }
    else if (state && priv.buttonFilter.buttonState && getDiffTime(priv.buttonFilter.startTime) > LONG_PRESS_TIME_US)
    {
        // Long Press
        priv.buttonFilter.startTime = (uint32_t)esp_timer_get_time();
        prevAction = PRESS_LONG;
        action = PRESS_LONG;
    }

    return action;
}

void startButtonThread(void *args)
{
    const TickType_t xDelay = 10 / portTICK_PERIOD_MS;   // 10 ms

    bool buttonState = false;
    BUTTON_ACTIONS_t buttonAction;
    uint8_t currBrightSet = 4;
    uint8_t maxBrightSet = 5;

    for(;;)
    {
        buttonState = getRawButtonState();
        buttonAction = buttonFilter(buttonState);
        if (buttonAction != PRESS_NONE)
        {
            printf("Button Action: %d \n", buttonAction);
        }
        
        if (buttonAction == RELEASE_SHORT)
        {
            LIGHTFESTIVAL_stopShow();
            GAMEHANDLER_requestStart();
        }
        else if (buttonAction == PRESS_LONG)
        {
            currBrightSet++;
            if (currBrightSet >= maxBrightSet)
            {
                currBrightSet = 0;
            }

            switch (currBrightSet)
            {
                case 0:
                LEDCONTROL_setBrightness(50);
                break;
                case 1:
                LEDCONTROL_setBrightness(150);
                break;
                case 2:
                LEDCONTROL_setBrightness(300);
                break;
                case 3:
                LEDCONTROL_setBrightness(600);
                break;
                case 4:
                LEDCONTROL_setBrightness(1023);
                break;
                
            }
            
            LEDCONTROL_switchOnLedNbrWithSetBrightness(LEDCONTROL_LED1);
            LEDCONTROL_switchOnLedNbrWithSetBrightness(LEDCONTROL_LED2);
            LEDCONTROL_switchOnLedNbrWithSetBrightness(LEDCONTROL_LED3);
            LEDCONTROL_switchOnLedNbrWithSetBrightness(LEDCONTROL_LED4);
            LEDCONTROL_switchOnLedNbrWithSetBrightness(LEDCONTROL_LED5);
            printf("Brightness: %d \n", currBrightSet);
        }

        if(GAMEHANDLER_isGameInProgress())
        {
            LEDCONTROL_switchOffLedNbr(LEDCONTROL_START_BTN_LED);
        }
        else
        {
            LEDCONTROL_switchOnLedNbr(LEDCONTROL_START_BTN_LED, 1023);
        }

        vTaskDelay( xDelay );
    }
}

// Functions --------------------------------------

void BUTTONHANDLER_init(void)
{
    xTaskCreate(&startButtonThread, "BUTTONTHREAD", STACK_SIZE_BUTTONHANDLER, NULL, PRIO_BUTTONHANDLER, NULL);
}